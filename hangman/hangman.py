'''
Wordlist dict is defined by WORDPATH[0:] = (DESCIPTIVE_NAME, ./RELATIVE/PATH/TO/FILE)
each line of wordlist file should be in this format
WORD:::HINT
'''
import random

WORDPATH = [
	('Progamming Languages', './pl.txt'),
	('House', './home.txt')
]
MAX_GUESS = 10

wordbank = dict()
guess = set()
score = 0
remaining_guess = MAX_GUESS

def parse_file(filename):
	f = open(filename, 'r+')
	for line in f:
		word,hint = line.strip().split(':::')
		wordbank[word] = hint

def play():
	while True:
		if(len(wordbank.keys()) == 0):
			print('No word can be played, please select other category')
			selectCategory()
			play()
		word = random.choice(list(wordbank.keys()))
		print("Hint: ", wordbank[word])
		origword = word
		word = word.lower()
		score = 0
		guess = set()
		remaining_guess = MAX_GUESS
		fin = True
		while remaining_guess > 0:
			fin = True
			for ch in word:
				print(ch if ch in guess or ord(ch) not in range(ord('a'), ord('z')+1) else '_', end=' ')
				if ch not in guess and ord(ch) in range(ord('a'), ord('z')+1):
					fin = False
			if fin:
				print("Congrat, your score is", score)
				break

			print('\t Incorrect guess: ')
			for ch in guess:
				if ch not in word:
					print(ch, end=' ')
			print('\n\t Score: ', score, '\n\t Remaining guess: ', remaining_guess)
			
			while True:
				tp = input('Type a character and press enter (only first character will be used): ').lower().strip()
				if(tp != "" and tp not in guess and ord(tp[0]) in range(ord('a'), ord('z')+1)):
					guess.add(tp[0])
					if tp not in word:
						remaining_guess -= 1
					for ch in word:
						if tp[0] == ch:
							score += 5
					break
				elif len(tp) == 0:
					print("Please type a letter before pressing enter")
				elif tp[0] in guess:
					print("Already guess this letter")
				elif ord(tp[0]) not in range(ord('a'), ord('z')+1):
					print("Please type only a-z character")
				
		
		if remaining_guess <= 0:
			print("You lose, your score is", score)

		c = input('Continue with this category? (y/N)').strip().lower()
		if(c == 'y'):
			del wordbank[origword]
			continue
		else:
			print("Bye!")
			break

def selectCategory():
	print("Select category by typing the number in front of the category name: ")
	for i in range(len(WORDPATH)):
		print(i, ':', WORDPATH[i][0])	
	
	while True:
		idx = int(input())
		if idx in range(len(WORDPATH)):
			parse_file(WORDPATH[idx][1])
			break
		else:
			print("Please type correct number for category name (0-", len(WORDPATH)-1, ")", sep="", end=" ")


selectCategory()
play()

