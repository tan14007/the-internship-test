## วิธีการใช้
1. รัน `npm install`
2. รันโปรแกรมโดยใช้คำสั่ง `node weather.js FILENAME`
3. ถ้าไม่มีข้อผิดพลาด โปรแกรมจะแสดงชื่อไฟล์ output นามสกุล json ออกมา

## Dependencies
- fs
- xml2js
- json-beautify