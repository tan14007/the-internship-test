var fs = require('fs');
var parseString = require('xml2js').parseString;
var beautify = require('json-beautify');

let ret = ""

if(process.argv.length != 3){
	console.log("USAGE: node weather.js INPUT.xml")
	return;
}

var content = fs.readFileSync(process.argv[2], 'utf8');

jsonobj = parseString(content);
fs.writeFileSync(process.argv[2].split('.')[0]+".json", beautify(jsonobj, null, 2, 50), (err) => {
  if (err){
  	console.log(err);
  	return;
  }
});

console.log(process.argv[2].split('.')[0]+".json");


